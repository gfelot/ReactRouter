import axios from 'axios'

export const FETCH_POSTS = 'FETCH_POSTS'
export const CREATE_POST = 'CREATE_POST'
export const GET_POST = 'GET_POST'
export const DELETE_POST = 'DELETE_POST'

const BaseURL = 'https://reduxblog.herokuapp.com/api'
const API_Key = '?key=ILoveReact'

export function fetchPosts() {
  const request = axios.get(`${BaseURL}/posts${API_Key}`)

  return {
    type: FETCH_POSTS,
    payload: request
  }
}

export function createPost(values, callback) {
  const request = axios.post(`${BaseURL}/posts${API_Key}`, values)
    .then(() => callback())

  return {
    type: CREATE_POST,
    payload: request
  }
}

export function getPost(id) {
  const request = axios.get(`${BaseURL}/posts/${id}${API_Key}`)

  return {
    type: GET_POST,
    payload: request
  }

}
export function deletePost(id, callback) {
  const request = axios.delete(`${BaseURL}/posts/${id}${API_Key}`)
    .then(() => callback())
  return {
    type: DELETE_POST,
    payload: id
  }
}
